/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : online_course_selection_system

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 02/03/2022 09:07:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' 管理员',
  `admin_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 管理员账号',
  `admin_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 管理员密码',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 角色',
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', '$2a$04$/b.UX.HUq.oFZnfqcTxzretcYuPeXbH2WyWRZcJKYDBGxSoXpHsZy', 'admin');

-- ----------------------------
-- Table structure for college
-- ----------------------------
DROP TABLE IF EXISTS `college`;
CREATE TABLE `college`  (
  `college_id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' 学院id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 学院名称',
  PRIMARY KEY (`college_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of college
-- ----------------------------
INSERT INTO `college` VALUES (1, '计算机学院');
INSERT INTO `college` VALUES (2, '中药学院');
INSERT INTO `college` VALUES (3, '外国语学院');
INSERT INTO `college` VALUES (4, '健康学院');
INSERT INTO `college` VALUES (5, '物理学院');
INSERT INTO `college` VALUES (6, '艺术学院');
INSERT INTO `college` VALUES (7, '信息学院');
INSERT INTO `college` VALUES (8, '化学学院');

-- ----------------------------
-- Table structure for selectable_course
-- ----------------------------
DROP TABLE IF EXISTS `selectable_course`;
CREATE TABLE `selectable_course`  (
  `course_id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' 课程id',
  `college_id` int(11) DEFAULT NULL COMMENT ' 学院id',
  `course_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 课程名称',
  `teacher` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 任课老师',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 课程类型',
  `course_score` int(11) DEFAULT 0 COMMENT ' 课程学分',
  `stock` int(11) UNSIGNED DEFAULT 0 COMMENT ' 库存量',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 上课地址',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 课程描述',
  `flag` int(1) DEFAULT 0 COMMENT '是否展示',
  PRIMARY KEY (`course_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of selectable_course
-- ----------------------------
INSERT INTO `selectable_course` VALUES (1, 1, '大学生创新创业指导', 'teacher1', '创新创业类', 2, 0, '星期三第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (2, 4, '人体的奥秘', 'teacher2', '自然科学类', 2, 27, '星期三第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (3, 3, '如何学好英语', 'teacher3', '公共艺术类', 2, 26, '星期三第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (4, 3, '婚恋指导', 'teacher4', '公共艺术类', 2, 34, '星期二第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (5, 6, '电影的艺术', 'teacher5', '限定艺术类', 2, 13, '星期四第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (6, 6, '戏剧欣赏', 'teacher6', '限定艺术类', 2, 57, '星期一第9-11节{4-15周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (7, 2, '中药理学', 'teacher7', '公共艺术类', 2, 48, '星期一第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (8, 1, '网页设计', 'teacher8', '自然科学类', 2, 43, '星期二第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (9, 1, '计算机网络导论', 'teacher9', '人文科学类', 2, 97, '星期四第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (10, 8, '化妆品学', 'teacher10', '公共艺术类', 2, 48, '星期三第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (11, 8, '化学原理', 'teacher11', '自然科学类', 2, 26, '星期二第9-11节{4-7周};', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (12, 1, '大学生计算机基础课程', 'teacher12', '公共艺术类', 2, 35, '星期三第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (13, 7, '人工智能概论', 'teacher13', '数学科学类', 2, 38, '星期四第9-11节{1-20周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (14, 6, '音乐鉴赏', 'teacher14', '限定艺术类', 2, 43, '星期一第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (15, 5, '牛顿力学解析', 'teacher15', '自然科学类', 2, 51, '星期二第9-11节{3-5周(单)', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (16, 5, '天文学', 'teacher16', '自然科学类', 2, 53, '星期六第1-4节{3-10周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (17, 8, '药品的解析', 'teacher17', '公共艺术类', 2, 37, '星期一第9-11节{3周,11周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (18, 1, '深入JVM', 'teacher18', '数学科学类', 2, 32, '星期四第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (19, 7, '爬虫原理', 'teacher19', '公共艺术类', 2, 30, '星期一第9-11节{3-14周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (20, 5, '浮力概述', 'teacher20', '自然科学类', 2, 39, '星期二第9-11节{4-7周};', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (21, 1, '深入分布式', 'teacher21', '公共艺术类', 2, 19, '星期二第9-11节{4-7周};', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (22, 1, 'Shiro与spring security', 'teacher22', '公共艺术类', 2, 11, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (23, 1, 'zookeeper', 'teacher23', '公共艺术类', 2, 34, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (24, 1, '有趣的python', 'teacher24', '公共艺术类', 2, 5, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (25, 4, '人体系统论', 'teacher25', '公共艺术类', 2, 73, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (26, 2, '药草认识大全', 'teacher26', '自然科学类', 2, 63, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (27, 4, '做一个健康的程序员', 'teacher27', '程序艺术类', 2, 47, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (28, 4, '脱发原理', 'teacher28', '公共艺术类', 2, 60, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (29, 5, '引力的奥秘', 'teacher29', '公共艺术类', 2, 33, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (30, 6, '论乐器', 'teacher30', '公共艺术类', 2, 44, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (31, 5, '观星学', 'teacher31', '公共艺术类', 2, 7, '星期日第1-3节{3-13周}', '帮助学生树立创新创业观念', 1);
INSERT INTO `selectable_course` VALUES (33, 1, 'test', 'test', '人文科学类', 3, 13, 'test', '', -1);
INSERT INTO `selectable_course` VALUES (34, 1, '344444', 'testteacg', '人文科学类', 1, 700, 'wqeqwe', 'qweqwewqeqweqwe', -1);
INSERT INTO `selectable_course` VALUES (35, 6, 'test35', 'testtest', '数学科学类', 3, 123, 'qweqweqw', '12312414124', -1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' user 自增id',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'user 姓名',
  `user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' user 账户',
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' user 密码',
  `user_object` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' user 专业',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 角色权限',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'student1', '123', '$2a$04$/b.UX.HUq.oFZnfqcTxzretcYuPeXbH2WyWRZcJKYDBGxSoXpHsZy', '计算机科学与技术', 'student');
INSERT INTO `user` VALUES (2, 'student2', '111', '$2a$04$/b.UX.HUq.oFZnfqcTxzretcYuPeXbH2WyWRZcJKYDBGxSoXpHsZy', '行政管理', 'student');
INSERT INTO `user` VALUES (3, 'student3', '222', '$2a$04$/b.UX.HUq.oFZnfqcTxzretcYuPeXbH2WyWRZcJKYDBGxSoXpHsZy', '临床医学', 'student');

-- ----------------------------
-- Table structure for user_course
-- ----------------------------
DROP TABLE IF EXISTS `user_course`;
CREATE TABLE `user_course`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT ' 自增id',
  `user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT ' 用户账户',
  `course_id` int(11) DEFAULT NULL COMMENT ' 课程id',
  `select_time` datetime(0) DEFAULT NULL COMMENT ' 选择时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_course
-- ----------------------------
INSERT INTO `user_course` VALUES (2, '111', 2, '2020-03-07 11:00:43');
INSERT INTO `user_course` VALUES (3, '222', 2, '2020-03-07 11:00:43');
INSERT INTO `user_course` VALUES (32, '222', 5, '2020-05-15 12:07:13');
INSERT INTO `user_course` VALUES (36, '111', 3, '2020-06-28 22:38:58');
INSERT INTO `user_course` VALUES (38, '222', 3, '2020-06-28 22:49:21');
INSERT INTO `user_course` VALUES (44, '111', 4, '2021-06-10 17:39:54');
INSERT INTO `user_course` VALUES (46, '123', 1, '2021-08-24 10:23:01');
INSERT INTO `user_course` VALUES (47, '123', 2, '2021-08-24 10:23:03');
INSERT INTO `user_course` VALUES (48, '123', 3, '2021-08-24 10:23:05');

SET FOREIGN_KEY_CHECKS = 1;
